<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class KategoriItem extends Model
{
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function item(){
        return $this->hasMany(Item::class);

    }
}
