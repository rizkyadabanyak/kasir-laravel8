@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h5>Item</h5>
                <a href="{{route('items.create')}}" class="waves-effect waves-light btn text-white ">Tambah</a>
            </div>
            <div class="card-body">
                <table >
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Roti</th>
                        <th scope="col">Rasa</th>
                        <th scope="col">Gambar</th>
                        <th scope="col">Harga</th>
                        <th scope="col">persediaan</th>
                        <th scope="col">action</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <th scope="col">{{$loop->iteration}}</th>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->kategori->nama}}</td>
                            <td>
                                <img src="{{asset($item->gambar)}}" width="50px" height="50px">
                            </td>
                            <td>Rp.{{$item->harga}}</td>
                            <td>{{$item->persediaan}}</td>
                            <td><a href="{{route('items.edit',$item->id)}}" class="waves-effect waves-light btn text-white blue lighten-1">Edit</a>
                                <a href="{{route('items.destroy',$item->id)}}" class="waves-effect waves-light btn red text-white">Delete</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection
