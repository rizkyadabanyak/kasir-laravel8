<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', [\App\Http\Controllers\HomeController::class,'index'])->name('home');

Route::post('/keranjang', [\App\Http\Controllers\ControllerKeranjang::class,'store'])->name('keranjang.store');
Route::patch('/keranjang/{keranjang}', [\App\Http\Controllers\ControllerKeranjang::class,'update'])->name('keranjang.update');
Route::delete('/keranjang/{keranjang}',[\App\Http\Controllers\ControllerKeranjang::class,'delete'])->name('keranjang.delete');

Route::post('/transaksi',[\App\Http\Controllers\ControllerTransaksi::class,'store'])->name('transaksi.store');
Route::get('/transaksi',[\App\Http\Controllers\ControllerTransaksi::class,'index'])->name('transaksi.index');
Route::get('/transaksi/{transaksis}',[\App\Http\Controllers\ControllerTransaksi::class,'show'])->name('transaksi.show');


Route::resource('items',\App\Http\Controllers\ItemController::class);
Route::get('/items/{id}/destroy',[\App\Http\Controllers\ItemController::class,'destroy'])->name('items.destroy');

Route::resource('categories',\App\Http\Controllers\CategoryController::class);
Route::get('/categories/{id}/destroy',[\App\Http\Controllers\CategoryController::class,'destroy'])->name('categories.destroy');
